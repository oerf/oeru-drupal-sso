OERu Drupal experiment


ToDo
1. create theme similar to OERu SilverStripe site, but responsive theme
2. create "Resource Bank" functionality (feature)
3. create mechanism to manage and efficiently create a Cohort of students
4. Single Sign-On either using CAS or Bakery modules
5. Create a Course functionality, with Course broken down into linked lessons.

Modules of interest:

Course:
Course - https://www.drupal.org/project/course
Course credit - https://www.drupal.org/project/course_credit
Course requirements - https://www.drupal.org/project/course_requirements
Course relationships - https://www.drupal.org/project/course_relationships

SSO:
https://groups.drupal.org/node/182004
Candidate: CAS
https://www.drupal.org/project/cas
https://www.drupal.org/project/cas_attributes
https://wiki.jasig.org/display/CASC/phpCAS
Candidate: Bakery
https://www.drupal.org/project/bakery
Candidate: SimpleSAML
https://blog.ctidigital.com/blog/setting-drupal-site-simplesamlphp-idp

