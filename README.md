# OERu SSO prototype using Drupal 7 #

This project describes a prototype functional website, built with the Drupal 7 platform, implementing a Single Sign-On front-end and both Service and Identity provider.

The site is intended to allow users to interact with their directory entries, and change things like their username (if allowed by directory maintainers) and email address. The site performs tasks like sending address validation/confirmation emails and allows users to request accounts (if configured to do so by directory maintainers). 

Designed to work with external LDAP directory.

### About this Repository ###

This repository contains

* the GPL-licensed source code for the Drupal 7 website.
* to complete the site, an instance of the database is required to complete the functionality (we will include a "cleaned" instance to remove user details) as Drupal includes configuration details for modules and themes in its database as well as content.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up for development? ###

* Recommended set up for development includes a Vagrant virtual machine running Ubuntu Linux 14.04  (reference to follow) providing the following services:
** nginx webserver (Apache2 can be used as an alternative)
** MariaDB database server (MySQL can be used as an alternative)
** PHP-FPM (in conjunction with nginx - if using Apache2, other PHP configurations can be used)
** an email server (to allow the site to send emails to test users)
* link the VMs web directory to a local volume holding the checkout of this project (or a fork if you want to commit your changes).

### How do I get set up for production? ###

* Setting up this Drupal site for Production, recommend using a Docker container running Ubuntu Linux 14.04 (reference to follow) running:
** nginx webserver (Apache2 can be used as an alternative)
** PHP-FPM (in conjunction with nginx - if using Apache2, other PHP configurations can be used)
* this Docker container should connect to the following services either running on the Docker host or separate data containers (to ease, for example, configuring database backups)
** MariaDB database server (MySQL can be used as an alternative)
** an email server (to allow the site to send emails to test users) - this can be configured using SSMTP to make use of an external email provider, e.g. SendGrid or Mandrill.

### Who do I talk to? ###

* talk to me, Dave Lane - https://davelane.nz or via my user on Bitbucket